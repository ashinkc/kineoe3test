drop schema IF EXISTS e3test;
create schema e3test;

use e3test;

CREATE TABLE company (
                         id INT AUTO_INCREMENT NOT NULL,
                         name VARCHAR(100),
                         PRIMARY KEY (id)
) ENGINE = InnoDB ROW_FORMAT = DEFAULT;

CREATE TABLE employee (
                          id INT AUTO_INCREMENT NOT NULL,
                          company_id INT NOT NULL,
                          first_name VARCHAR(100),
                          last_name VARCHAR(100),
                          PRIMARY KEY (id),
                          FOREIGN KEY (company_id) REFERENCES company(id)
) ENGINE = InnoDB ROW_FORMAT = DEFAULT;


insert into company values (1, 'Kineo');
insert into company values (2, 'Hungry Jacks');

insert into employee values (1, 1, 'Joe', 'Jones');
insert into employee values (2, 2, 'Bob', 'Brown');
insert into employee values (3, 2, 'Annie', 'Armstrong');
insert into employee values (4, 1, 'Susan', 'Smith');




select * from employee;

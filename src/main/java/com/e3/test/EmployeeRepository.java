package com.e3.test;

import com.e3.test.model.Company;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.e3.test.model.Employee;


import java.util.List;
import java.util.Optional;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long> {
    Optional<Employee> findById(Long id);
    List<Employee> findByIdOrFirstNameOrLastNameOrderByFirstNameAsc(Long employeeId, String firstName, String lastName);
}

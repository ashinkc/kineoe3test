package com.e3.test;

import com.e3.test.model.Company;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import com.e3.test.model.Employee;

import java.util.List;
import java.util.Optional;

@RestController
public class EmployeeController {

	@Autowired
	EmployeeRepository employeeRepository;

	@Autowired
	CompanyRepository companyRepository;


	@GetMapping("/employee/{employeeId}")
	public ResponseEntity<Employee> getEmployeesByCompany(@PathVariable Long employeeId) {
		Optional<Employee> employeeData = employeeRepository.findById(employeeId);
		if(employeeData.isPresent()){
			return new ResponseEntity<>(employeeData.get(), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@GetMapping("/employee")
	public ResponseEntity<List<Employee>> getAllEmployees() {
		List<Employee> employeeData = employeeRepository.findAll();
		if(!employeeData.isEmpty() && employeeData != null){
			return new ResponseEntity<>(employeeData, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}


	@PostMapping("/employee")
	public ResponseEntity<Employee> createEmployee(@RequestBody Employee employee){
			if(!validEmployeeNames(employee)){
				return new ResponseEntity<>(employee,HttpStatus.BAD_REQUEST);
			}
			Optional<Company> company = companyRepository.findById(employee.getCompany().getId());
			try {
				Employee _employee = new Employee();
				_employee.setFirstName(employee.getFirstName());
				_employee.setLastName(employee.getLastName());
				_employee.setCompany(employee.getCompany());
				return new ResponseEntity<>(employeeRepository.save(_employee), HttpStatus.CREATED);
			} catch (Exception e) {
				return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
			}
	}



	@PutMapping("/employee/{employeeId}")
	public ResponseEntity<Employee> saveEmployee(@PathVariable("employeeId") Long employeeId, @RequestBody Employee employee){
		Optional<Employee> employeeData = employeeRepository.findById(employeeId);
		if(!validEmployeeNames(employee)){
			return new ResponseEntity<>(employee,HttpStatus.BAD_REQUEST);
		}
		if(employeeData.isPresent()){
			Employee _employee = employeeData.get();
			_employee.setFirstName(employee.getFirstName());
			_employee.setLastName(employee.getLastName());
			_employee.setCompany(employee.getCompany());
			return new ResponseEntity<>(employeeRepository.save(_employee),HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@DeleteMapping("/employee/{employeeId}")
	public ResponseEntity<String> deleteEmployee(@PathVariable("employeeId") long employeeId){
		try{
			employeeRepository.delete(employeeId);
			return new ResponseEntity<>("Delete operation successful",HttpStatus.OK);
		} catch (Exception exception){
			return new ResponseEntity<>("Delete Operation Not Successful",HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/employee/search")
	public ResponseEntity<List<Employee>> searchEmployee(@RequestParam(name = "employeeId", required = false) Long employeeId,@RequestParam(name = "firstName",required = false) String firstName,
														 @RequestParam(name = "lastName",required = false) String lastName, @RequestParam(name = "companyId", required = false) Long companyId,
														 @RequestParam(name = "companyName", required = false) String companyName){
		try{
			List<Employee> employeeData = employeeRepository.findByIdOrFirstNameOrLastNameOrderByFirstNameAsc(employeeId,firstName,lastName);
			return new ResponseEntity<>(employeeData,HttpStatus.OK);
		} catch (Exception exception){
			return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	public static boolean validEmployeeNames(Employee employee){
		if(employee.getFirstName().matches("[a-zA-Z-' ]*")&&employee.getLastName().matches("[a-zA-Z-' ]*")) {
			System.out.println("String check match:::: true");
			return true;
		}
		return false;
	}


}

package com.e3.test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import com.e3.test.model.Company;

import java.util.List;
import java.util.Optional;

@RestController

public class CompanyController {

	@Autowired
	CompanyRepository companyRepository;


	@GetMapping("/company/{companyId}")
	public ResponseEntity<Company> getCompany(@PathVariable Long companyId) {
		Company companyData = companyRepository.findOne(companyId);

		if(companyData != null){
			companyData.getEmployee().sort(((employee1, employee2) -> employee1.getFirstName().compareTo(employee2.getFirstName())));
			return new ResponseEntity<>(companyData,HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

	}


	@GetMapping("/company")
	public ResponseEntity<List<Company>> getAllCompany() {
		List<Company> companyData = companyRepository.findAll();

		if(!companyData.isEmpty()){
			return new ResponseEntity<>(companyData,HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

	}


	@PostMapping("/company")
	public ResponseEntity<Company> createCompany(@RequestBody Company company) {

		try{
			Company _comp = new Company();
			_comp.setName(company.getName());
			return new ResponseEntity<>(companyRepository.save(_comp),HttpStatus.CREATED);
		} catch (Exception e){
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	@PutMapping("/company/{id)")
	public ResponseEntity<Company> updateCompany(@RequestBody Company company, @PathVariable("id") Long id){
		Optional<Company> companyData = companyRepository.findById(id);
		if(companyData.isPresent()){
			Company _company = companyData.get();
			_company.setName(company.getName());
			return new ResponseEntity<>(company,HttpStatus.OK);
		} else {
			return new ResponseEntity(HttpStatus.NOT_FOUND);
		}
	}

	@DeleteMapping("/company/{id}")
	public ResponseEntity<String> deleteCompany(@PathVariable("id") Long id) {
		Optional<Company> company = companyRepository.findById(id);
		if(! company.isPresent() || company.get().getEmployee().isEmpty()){
			return new ResponseEntity<>("Company with employees can not be deleted",HttpStatus.INTERNAL_SERVER_ERROR);
		} else {
			try {
				companyRepository.delete(id);
				return new ResponseEntity<>("Delete Operation Successful", HttpStatus.OK);
			} catch (Exception e) {
				return new ResponseEntity<>("Delete Operation Not Successful", HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}
	}


}
